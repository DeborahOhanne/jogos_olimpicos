# API - Jogos Olímpicos

## Inicio
 - Projeto desenvolvido em [Python](https://www.python.org/) 
 - Esse projeto foi desenvolvido utilizando [Django](https://docs.djangoproject.com/)
 - Criação de uma API REST para o COB (Comitê Olímico Brasileiro), que será responsável por marcar e   dizer os vencedores das seguintes modalidades:
    - 100m rasos: Menor tempo vence
    - Lançamento de Dardo: Maior distância vence

## Principais recursos utilizados no desenvolvimento:

- Python
- Django
- SQLite


### Pré-requisitos
Todo o projeto foi desenvolvido utilizando a linguagem de programação Python.
As bibliotecas utilizadas durante o desenvolvimento encontram-se citadas no arquivo _requirements.txt_ para rodar o projeto é necessário a instalação das bibliotecas.

Todo o projeto foi adaptado para rodar em containers do Docker. Então, para levantar a aplicação localmente é necessário ter instalado o [docker](https://docs.docker.com/install/) e o [docker-compose](https://docs.docker.com/compose/install/)


Depois de instalado é necessário construir a imagem docker da API do projeto.
Para construir a imagem execute o comando abaixo:

```
docker-compose build
```

É possível verificar se as imagem foi construída com sucesso.
Execute o comando abaixo:

```
docker image ls
```

### Iniciar a aplicação
Para o projeto funcionar, é necessário subir o docker. Para isso, basta levantar o container através do docker-compose.
Execute o comando abaixo:

```
docker-compose up
```


### Utilização

É possível testar a aplicação de 2 formas:

- Fazer requisições pela documentação do projeto: http://0.0.0.0:8000/docs/

- Enviar requisições no [POSTMAN](https://www.postman.com/downloads/)

