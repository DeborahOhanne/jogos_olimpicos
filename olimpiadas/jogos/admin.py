from django.contrib import admin
from jogos.models import Competicao, Atleta, Resultado


admin.site.register(Competicao)
admin.site.register(Atleta)
admin.site.register(Resultado)