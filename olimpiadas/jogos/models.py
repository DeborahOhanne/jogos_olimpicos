from django.db import models
from rest_framework.exceptions import ValidationError
from django.shortcuts import get_object_or_404


class Competicao(models.Model):

    STATUS_COMPETICAO = (
        ('1', 'Aguardando início'),
        ('2', 'Competição em andamento'),
        ('0', 'Competição Encerrada'),
    )

    CONDICAO_PARA_VITORIA = (
        ('maior', 'maior_resultado vence'),
        ('menor', 'menor resultado vence'),
        ('maior_de_tres', 'maior de três resultados vence'),
        ('menor_de_tres', 'menor de três resultados vence'),
    )

    nome = models.CharField('Nome da competição', max_length=100)
    condicao_para_vitoria = models.CharField('Condição para vitória da competição', max_length=13, choices=CONDICAO_PARA_VITORIA)
    status = models.CharField('Status da competição', max_length=10, choices=STATUS_COMPETICAO, default='1')

    def __str__(self):
        return self.nome


class Atleta(models.Model):

    nome = models.CharField('Nome do/da atleta', max_length=200, unique=True)

    def __str__(self):
        return self.nome


class Resultado(models.Model):
    UNIDADE = (
        ('m', 'metros'),
        ('s', 'segundos'),
    )

    competicao = models.ForeignKey(Competicao, on_delete=models.CASCADE)
    atleta = models.ForeignKey(Atleta, on_delete=models.CASCADE)
    resultado = models.DecimalField('Resultado obtido pelo atleta', max_digits=8, decimal_places=3)
    unidade = models.CharField(max_length=13, choices=UNIDADE)
    tentativa = models.IntegerField(default=1)

    def save_resultado(self):
        ultimo_resultado = Resultado.objects.filter(atleta_id=self.atleta.id, competicao_id=self.competicao.id).last()

        if ultimo_resultado:
            self.tentativa = ultimo_resultado.tentativa+1

        # ao atingir o número máximo de tantativas, a inserção do dado no banco não é realizada e gera um erro informando
        if self.tentativa > 3:
            raise ValidationError(detail='Só são permitidas 3 tentativas por atleta')

        # a API não deve aceitar cadastros de resultados se a competição já estiver encerrada
        if self.competicao.status == '0':
            raise ValidationError(detail='A competição já foi encerrada')

        super(Resultado, self).save()

    def melhor_resultado_competicao(self, resultados):
        ranking = []
        atletas = []

        for result in resultados:
            atleta = result.atleta_id
            if atleta not in atletas:
                atletas.append(atleta)
                ranking.append(result)

        return ranking

    def rank_competicao(self, id_competicao):

        competicao = get_object_or_404(Competicao, id=id_competicao)

        if competicao.condicao_para_vitoria in ('menor', 'menor_de_tres'):
            order_by = r'resultado'
        else:
            order_by = r'-resultado'

        resultados = Resultado.objects.filter(competicao=competicao.id).order_by(order_by)

        return self.melhor_resultado(resultados)

    def __str__(self):
        string = '{} - {} - {} {}'.format(self.competicao, self.atleta, self.resultado, self.unidade)

        return string
