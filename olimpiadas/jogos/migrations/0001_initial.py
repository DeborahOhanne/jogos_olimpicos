# Generated by Django 3.2.8 on 2021-10-08 02:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Atleta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=200, unique=True, verbose_name='Nome do atleta')),
            ],
        ),
        migrations.CreateModel(
            name='Competicao',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100, verbose_name='Nome da competição')),
                ('condicao_para_vitoria', models.CharField(choices=[('maior', 'Maior_resultado vence'), ('menor', 'Menor resultado vence'), ('maior_de_tres', 'Maior de três resultados vence'), ('menor_de_tres', 'Menor de três resultados vence')], max_length=13, verbose_name='Condição para vitória')),
                ('status', models.CharField(choices=[('1', 'Aguardando início'), ('2', 'Competição em andamento'), ('0', 'Competição Encerrada')], default='1', max_length=10, verbose_name='Status da competição')),
            ],
        ),
        migrations.CreateModel(
            name='Resultado',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resultado', models.DecimalField(decimal_places=3, max_digits=8, verbose_name='Valor do resultado obtido pelo atleta')),
                ('unidade', models.CharField(choices=[('m', 'Metros'), ('s', 'Segundos')], max_length=13)),
                ('tentativa', models.IntegerField(default=1)),
                ('atleta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jogos.atleta')),
                ('competicao', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jogos.competicao')),
            ],
        ),
    ]
