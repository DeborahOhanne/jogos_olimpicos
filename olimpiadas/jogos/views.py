from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from jogos.models import Competicao, Atleta, Resultado
from jogos.serializers import CompeticaoSerializer, AtletaSerializer, ResultadoSerializer, RankingSerializer


class CompeticaoView(APIView):

    def get(self, request, format=None):
        queryset = Competicao.objects.all()
        serializer_class = CompeticaoSerializer(queryset, many=True)
        return Response(serializer_class.data)

    def post(self, request, format=None):
        serializer = CompeticaoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AtletaView(APIView):

    def get(self, request, format=None):
        queryset = Atleta.objects.all()
        serializer_class = AtletaSerializer(queryset, many=True)
        return Response(serializer_class.data)

    def post(self, request, format=None):
        serializer = AtletaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResultadoView(APIView):

    def get(self, request, format=None):
        queryset = Resultado.objects.all()
        serializer_class = ResultadoSerializer(queryset, many=True)
        return Response(serializer_class.data)

    def post(self, request, format=None):
        serializer = ResultadoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RankingView(APIView):

    def get(self, request, pk):
        resultados = Resultado.pegar_rank_da_competicao(pk)

        serializer = RankingSerializer(resultados, many=True)

        return Response(serializer.data)